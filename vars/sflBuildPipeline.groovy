//
// pipeline parameters:
//  projectName: path/to/reponame
//  agentLabel: for agent{}
//  prepCommand: first command to run
//  buildCommand: 2nd command to run
def call(body) {
    def pipelineParams= [:]
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = pipelineParams
    body()

    pipeline {
        agent { label "${pipelineParams.agentLabel}" }
    
        stages {
            stage('Prep') {
                steps {
                    sh 'env | sort'
                    sh "${pipelineParams.prepCommand}"
                }
    
            } // stage('Prep')

            stage('Build') {
                steps {
                    checkout scm
    
                    sh "${pipelineParams.buildCommand}"
                    archiveArtifacts artifacts: "${pipelineParams.buildArtifacts}"
                }
            } // stage('Build')
        } // stages{}

        post {
            success {
                gerritReview labels: [Verified: 1]
                    gerritCheck checks: ['example:checker': 'SUCCESSFUL']
            }
            unstable { gerritReview labels: [Verified: 0], message: 'Build is unstable' }
            failure {
                    gerritReview labels: [Verified: -1]
                    gerritCheck checks: ['example:checker': 'FAILED'], message: 'invalid syntax'
            }
        } // post{}
    }
}
